EXCUSES DE DEV

Application web développée avec Symfony, Twig/CSS et Javascript natif 
qui génère des phrases pour faire patienter les utilisateurs.

PREREQUIS:

-PHP 7 ou supérieur
-Composer
-Une base de donnée SQL

INSTALLATION:

- Cloner le dépôt git: git clone git@gitlab.com:Diabo7/dev-excuses.git
- Accéder au répertoire du projet
- Installer les dépendances PHP avec Composer: composer install
- Lancer les scripts d'initialisation puis création de la bdd présents
à la racine du projet

LANCEMENT:

- Démarer le serveur symfony intégré: symfony server:start
- Lancer votre base de donnée