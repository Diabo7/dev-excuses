let modal = document.getElementById("myModal");
let button = document.getElementById("openOrClose");
let listExcuses = document.getElementById("listExcuses");

function displayModal() {
    if (button.dataset.open == 0) {
        button.dataset.open = 1;
        modal.style.display = "block";
        listExcuses.style.opacity = "0.2";
    } else {
        button.dataset.open = 0;
        modal.style.display = "none";
        listExcuses.style.opacity = "1";
    }
}

document.addEventListener("keydown", (event) => {
    if (button.dataset.open != 0) {
        if (event.keyCode == 27) {	// Escape key.
            displayModal();
        }
    }
})
