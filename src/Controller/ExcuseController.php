<?php

namespace App\Controller;

use App\Repository\ExcuseRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Excuse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ExcuseType;

class ExcuseController extends AbstractController
{
    private $previouslyPicked = "";

    #[Route("/", name: "app_index")]
    public function index(ExcuseRepository $repo): Response
    {
        $excuses = $repo->findAll();

        return $this->render('base.html.twig', [
            "excuses" => $excuses
        ]);
    }

    #[Route('/lost', name: "app_lost")]
    public function lost(): Response
    {
        return $this->render("lost/lost.html.twig");
    }

    #[Route("/random", name: "app_random")]
    public function random(ExcuseRepository $repo): Response
    {
        $excusesList = $repo->findAll();
        do {
            $randomNumber = rand(0, count($excusesList) - 1);
        } while ($randomNumber == $this->previouslyPicked);
        $this->previouslyPicked = $randomNumber;
        $excuses = array($excusesList[$randomNumber]);

        return $this->render('excuse/index.html.twig', [
            "excuses" => $excuses
        ]);
    }

    #[Route("/add", name: "app_add")]
    public function add(EntityManagerInterface $entityManager, Request $request, ExcuseRepository $repo): Response
    {
        $excuse = new Excuse();
        $form = $this->createForm(ExcuseType::class, $excuse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($repo->findOneBy(["http_code" => $form->get('http_code')->getData()])) {
                return $this->render('excuseForm/index.html.twig', [
                    'form' => $form->createView(),
                    "error" => "Invalid Code"
                ]);
            } else {
                $excuse->setHttpCode($form->get('http_code')->getData());
                $excuse->setMessage($form->get('message')->getData());
                $entityManager->persist($excuse);
                $entityManager->flush();

                return new Response("Successfully added");
            }
        }

        return $this->render('excuseForm/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route("/{http_code}", name: "app_message")]
    public function message($http_code, ExcuseRepository $repo): Response
    {
        $excuses = $repo->findBy(["http_code" => $http_code]);
        if ($excuses) {
            return $this->render('excuse/index.html.twig', [
                "excuses" => $excuses
            ]);
        } else {
            return $this->render('bundles/TwigBundle/Exception/error404.html.twig', [
                "excuses" => $excuses
            ]);
        }
    }
}
